<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); 

    $_post_training = get_field('post_szkolenie');
    $_category_fields = get_field('grupa_szkolen_kategorie','category_'.returnCategory(get_the_terms( $post->ID, 'category' ))); 
  


?>

<section id="primary" class="content-area">
  <main id="main" class="site-main " role="main">
    <div class="heading">
      <div class="icon">
        <?php 
                            if($_post_training['ikona_postu']){
                                echo '<img src="'.$_post_training['ikona_postu']['url'].'"/ alt="'.$_post_training['ikona_postu']['alt'].'">';
                            } 
                            ;?>
      </div>
      <div class="after-main-icon">
        <div class="left-column">
          <h1 class="title">
            <?php 
				the_title();
			;?>
          </h1>
          <p class="gold text">Szkolenie</p>
        </div>
        <div class="right-column">
          <?php if($_post_training['cena']): ;?>
          <div class="gold price">
            <?php echo $_post_training['cena'];?>
            <span class="gold">netto</span>
          </div>
                          <?php endif;
     
      ;?>
		  <div class="w-cta">
			<p class="cta gold js-modal-training" data-toggle="modal" data-target="#modalTrainers" data-nameTrainers="<?php echo trim($post->post_title);?>" data-dataTrainers="<?php echo $_post_training['data_od'].' - '.$_post_training['data_do'];?>">zapisz się</p>
			</div>
        </div>
      </div>
    </div>
    <div class="w-post">
      <?php if($_post_training['opis_szkolenia']): ;?>
      <p class="text">
        <?php echo $_post_training['opis_szkolenia'];?>
      </p>
      <?php endif;?>
      <?php
		while ( have_posts() ) : the_post();

			the_content();

		endwhile; // End of the loop.
		?>
	</div>
	
	<div class="trainer-icons">
    <p class="title">Nasi prowadzący</p>
    <div class="w-icons">
    <?php foreach ( $_post_training['ikony_prowadzacych'] as $row_p ): ;?>
      <p class="icon" style="background: url('<?php echo $row_p['ikona']['url'];?>') no-repeat"></p>
    <?php endforeach ;?>
    </div>
	</div>

	<div class="section-encourage">
		<div class="left-colum">
			<p class="title"><?php the_title();?></p>
			<p class="gold text">Szkolenie</p>
			<?php if($_post_training['zapisz_sie_text_zachecajacy']): ;?>
			<p class="text"><?php echo $_post_training['zapisz_sie_text_zachecajacy'] ;?></p>
			<?php endif;?>
		</div>
		<div class="right-column">
          <?php if($_post_training['cena']): ;?>
          <div class="gold price">
            <?php echo $_post_training['cena'];?>
            <span class="gold">netto</span>
          </div>
		  <?php endif;?>
		  <div class="w-cta">
			<p class="cta gold js-modal-training" data-toggle="modal" data-target="#modalTrainers" data-nameTrainers="<?php echo trim($post->post_title);?>" data-dataTrainers="<?php echo $_post_training['data_od'].' - '.$_post_training['data_do'];?>">zapisz się</p>
			</div>
        </div>
	</div>
	<div class="w-gallery">
               <div class="header">
               <img src="<?php echo $_post_training['ikona_galerii']['url'] ;?>" alt="<?php echo $_site_models['ikona_strony']['alt'] ;?>" class="icon">
                <p class="title">Galeria zdjęć ze szkoleń</p>
                
               </div>
               <?php if( $_category_fields['galeria']) :;?>
               
                <div class="gallery">
                    
                <?php foreach (  $_category_fields['galeria'] as $row_gallery ): ;?>
                    <div class="item-gallery bg-display-image" style="background: url('<?php echo $row_gallery ['url'];?>') no-repeat">
                       
                    </div>
                <?php endforeach ;?>
                
                </div>
                <?php endif;?>
           </div>
  </main><!-- #main -->
</section><!-- #primary -->
<?php 

;?>
<div id="modalTrainers" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Zapisz się na szkolenie</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
       <?php echo do_shortcode('[contact-form-7 id="303" title="Szkolenie"]');?>
      </div>
    </div>
  </div>
</div>
<?php
get_footer();