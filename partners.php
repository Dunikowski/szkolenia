<?php
/**
 * Template Name: Partners
 */

get_header();

if(get_field('partnerzy') !== null) :
    $_section_partners = get_field('partnerzy');
?>
    <section class="parners">
        <div class="w-content">
            <?php
                while ( have_posts() ) : the_post();
                   ?> 
                <div class="heading">
                    <div class="icon">
                        <?php 
                            if($_section_partners['ikona_strony']){
                                echo '<img src="'.$_section_partners['ikona_strony']['url'].'"/ alt="'.$_section_partners['ikona_strony']['alt'].'">';
                            } 
                            ;?>
                        </div>
                        <h1 class="title">
                        <?php 
                            the_title();
                            ;?>
                        </h1>
                    </div>
                    <div class="content-parners">
                        <?php the_content();?>                    
                    </div>
                    <?php
                endwhile; // End of the loop.
            ?>
            <?php if($_section_partners['partnerzy_brands'] !== null):;?>
            <div class="w-brands">
            <?php foreach ( $_section_partners['partnerzy_brands'] as $row ): ;?>
                <a class="item-brand" href="<?php echo $row['hiperlacze'];?>" rel="nofollow" style="background: #1f2125 url(' <?php echo $row['brand']['url'];?>') no-repeat" alt="<?php echo $row['brand']['alt'];?>">
        
            </a>
            <?php endforeach ;?>
            </div>
            <?php endif;?>
            <?php if($_section_partners['zaproszenie_do_wspolpracy_text'] !== null):;?>
            <div class="w-cooperation">
                <div class="cooperation">
                <p class="title">
                    Zapraszamy do współpracy
                </p>
                <p class="text">
                <?php echo $_section_partners['zaproszenie_do_wspolpracy_text'];?>
                </p>
                </div>
                <a href="<?php echo $_section_partners['hiperlacze_kontakt'];?>" rel="follow" class="gold cta">kontakt</a>
            </div>
            <?php endif;?>
        </div>
        
    </section><!-- #primary -->

<?php
endif;
get_footer();
