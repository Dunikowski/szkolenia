jQuery(document).ready(function(){

    var _b_modal = jQuery('.js-modal-training');
    if(_b_modal.length > 0) {
        _b_modal.on('click', function(){

            var _this = jQuery(this);
            var nameTrainers = _this.data('nametrainers');
            var date = _this.data('datatrainers');
            jQuery('form [name="Kurs"]').val(nameTrainers);
            jQuery('form [name="Datakursu"]').val(date);
        });
    }
    
// input file update

var inputs = jQuery( '.i-files' );
Array.prototype.forEach.call( inputs, function( input )
{
   
  
	var label	 = jQuery(input).parent().siblings('label'),
		labelVal = label.innerHTML;
      
	input.addEventListener( 'change', function( e )
	{
		var fileName = '';
		if( this.files && this.files.length > 1 ){
            fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
           
        } else{
            fileName = e.target.value.split( '\\' ).pop();
           
        }
			
		if( fileName ){
            label[0].innerHTML = fileName;
            
        }
		 else {
            label[0].innerHTML = labelVal;
         }
		 	
	});
});
// Add close to menu
var _w = jQuery(document).width();
if (_w < 1200) {
   var _nav = jQuery('#main-nav');
   if(_nav.find("span.close").length == 0){
    _nav.prepend("<span class='close'></span>");
   }
   var _span = _nav.find("span.close");
   if(_span.length > 0){
    _span.on("click", function(){
       _nav.siblings('button.navbar-toggler').click();
       
    });
   }
}
});