const gulp = require('gulp');
const sass= require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const rename      = require('gulp-rename');
//const watchSass = require("gulp-watch-sass");
const browserSync = require('browser-sync').create();
 const uglify = require("gulp-uglify");
 var pipeline = require('readable-stream').pipeline;
// const babel = require("gulp-babel");
// const concat = require("gulp-concat");


const config= {_js:"./inc/assets/js",_scss:"./inc/assets/scss",_css:"./inc/assets/css"};
//   gulp.task("compress", function() {
   
//     return pipeline(
//       gulp.src('./template/klinika-ekspresow/js/slideshow.js'),
//       uglify(),
//       rename({suffix: '.min'}),
//       gulp.dest('./template/klinika-ekspresow/js')
// );
      
//   });

  gulp.task('sass', function(){
    
    return gulp.src('./inc/assets/scss/*.scss')
      .pipe(sass({outputStyle: 'compressed'})) // Using gulp-sass
      .pipe(sourcemaps.init())
      .pipe(autoprefixer({
        browsers: ['last 10 versions'],
        cascade: false
    }))
      .pipe(rename({suffix: '.min'}))
      .pipe(gulp.dest('./inc/assets/css'))
  });

gulp.task('default',['sass'],function(){
  browserSync.init({
    watchTask: true,
    proxy: "localhost/szkolenia"
});
});
gulp.watch('./inc/assets/scss/*.scss', ['sass']).on('change', browserSync.reload);
 gulp.watch('./*.php').on('change', browserSync.reload);
//gulp.watch("./template/klinika-ekspresow/css/*.css", ['sass']);
//gulp.watch("./template/klinika-ekspresow/js/slideshow.js", ['compress']);
