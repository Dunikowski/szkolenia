<?php

get_header();

?>
<div  id="team" class="team col-xs-12">
  <?php
  if(get_field('sekcja_zespol',6) !== null) :
    $_section_team = get_field('sekcja_zespol',6);
  ?>
  <div class="time-content">
    <div class="icon">
      <?php 
           if($_section_team['ikona']){
            echo '<img src="'.$_section_team['ikona']['url'].'"/>';
           } 
           ;?>
    </div>
    <h2 class="title">
      <?php 
           if($_section_team['ikona']){
            echo $_section_team['tytul'];
           } 
           ;?>
    </h2>
    <p class="text">
      <?php 
           if($_section_team['ikona']){
            echo $_section_team['text'];
           } 
           ;?>
    </p>
    <a href="<?php  echo $_section_team['zobacz_wiecej_hiper_lacze'];?>" class="gold" rel="nofollow">
      przeczytaj więcej
    </a>
  </div>
</div>
</div><!-- end row --->
</div><!-- end w-content --->
</div><!-- end w-content --->

<?php if( get_field('sekcja_zespol',6)['workeres']) : ;?>

   <div class="team-persons">
   
   <?php foreach ( $_section_team['workeres'] as $row ): ;?>
   <div class="item-person" style="background: url(' <?php echo $row['tlo']['url'];?>') no-repeat">
      <div class="w-info">
         <p class="name"><?php echo $row['name'];?></p>
         <p class="alias"><?php echo $row['alias'];?></p>
         <p class="text"><?php echo $row['text'];?></p>
      </div>
   </div>
   <?php endforeach ;?>
   </div>
<?php endif ;?>


      <!-- end w-content --->
      <?php endif; ?>
      <div class="site-content">
  <div class="container w-content">
    <div class="row">
    <?php
  if(get_field('widok_strony_glownej_szkolenia',8) !== null) :
    $_section_training = get_field('widok_strony_glownej_szkolenia',8);
  ?>
      <section class="training col-xs-12">
        <div class="header">
          <img src="<?php echo $_section_training['ikona_szkolenia']['url'] ;?>" alt="Ikona Szkolenia" class="icon">
        <h2 class="title"><span>Szkolenia</span><span class="gold">fryzjerskie</span></h2>
        <?php echo apply_filters('the_content',get_post_field('post_content',8));?>
        </div>
      <div class="content-training">
      <?php foreach ( $_section_training['szkolenie'] as $row_t ): 
      $row_t_p = get_field('post_szkolenie',$row_t['szkolenie_wpis']->ID);
     
        ;?>
      
      <div class="item-training" >
            <p class="icon" style="background: url('<?php echo $row_t_p['ikona_prowadzacego_podstrona_szkolenia_oraz_glowna']['url'];?>') no-repeat"></p>
            <div class="upcoming-training">
              <p class="text">Najblizsze szkolenie</p>
              <div class="date">
              <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
              width="16" height="16" viewBox="0 0 489.2 489.2" style="enable-background:new 0 0 489.2 489.2;" xml:space="preserve">
                <g>
                  <g>
                    <path d="M177.8,238.1c0,4.5-3.6,8.1-8.1,8.1h-30.4c-4.5,0-8.1-3.6-8.1-8.1v-30.4c0-4.5,3.6-8.1,8.1-8.1h30.4
                      c4.5,0,8.1,3.6,8.1,8.1V238.1z M241.3,207.8c0-4.5-3.6-8.1-8.1-8.1h-30.4c-4.5,0-8.1,3.6-8.1,8.1v30.4c0,4.5,3.6,8.1,8.1,8.1h30.4
                      c4.5,0,8.1-3.6,8.1-8.1V207.8z M304.8,207.8c0-4.5-3.6-8.1-8.1-8.1h-30.4c-4.5,0-8.1,3.6-8.1,8.1v30.4c0,4.5,3.6,8.1,8.1,8.1h30.4
                      c4.5,0,8.1-3.6,8.1-8.1V207.8z M177.8,269.6c0-4.5-3.6-8.1-8.1-8.1h-30.4c-4.5,0-8.1,3.6-8.1,8.1V300c0,4.5,3.6,8.1,8.1,8.1h30.4
                      c4.5,0,8.1-3.6,8.1-8.1V269.6z M241.3,269.6c0-4.5-3.6-8.1-8.1-8.1h-30.4c-4.5,0-8.1,3.6-8.1,8.1V300c0,4.5,3.6,8.1,8.1,8.1h30.4
                      c4.5,0,8.1-3.6,8.1-8.1V269.6z M296.7,261.5h-30.4c-4.5,0-8.1,3.6-8.1,8.1V300c0,4.5,3.6,8.1,8.1,8.1h30.4c4.5,0,8.1-3.6,8.1-8.1
                      v-30.4C304.8,265.1,301.2,261.5,296.7,261.5z M106.1,323.3H75.8c-4.5,0-8.1,3.6-8.1,8.1v30.4c0,4.5,3.6,8.1,8.1,8.1h30.4
                      c4.5,0,8.1-3.6,8.1-8.1v-30.4C114.3,326.9,110.6,323.3,106.1,323.3z M114.3,269.6c0-4.5-3.6-8.1-8.1-8.1H75.8
                      c-4.5,0-8.1,3.6-8.1,8.1V300c0,4.5,3.6,8.1,8.1,8.1h30.4c4.5,0,8.1-3.6,8.1-8.1V269.6z M233.2,323.3h-30.4c-4.5,0-8.1,3.6-8.1,8.1
                      v30.4c0,4.5,3.6,8.1,8.1,8.1h30.4c4.5,0,8.1-3.6,8.1-8.1v-30.4C241.3,326.9,237.7,323.3,233.2,323.3z M169.7,323.3h-30.4
                      c-4.5,0-8.1,3.6-8.1,8.1v30.4c0,4.5,3.6,8.1,8.1,8.1h30.4c4.5,0,8.1-3.6,8.1-8.1v-30.4C177.8,326.9,174.2,323.3,169.7,323.3z
                      M360.2,246.3c4.5,0,8.1-3.6,8.1-8.1v-30.4c0-4.5-3.6-8.1-8.1-8.1h-30.4c-4.5,0-8.1,3.6-8.1,8.1v30.4c0,4.5,3.6,8.1,8.1,8.1H360.2
                      z M47.7,435.9h230.7c-3.7-11.6-5.8-24-5.9-36.8H47.7c-6,0-10.8-4.9-10.8-10.8V171h361.7v101.1c12.8,0.1,25.2,2,36.8,5.7V94.9
                      c0-26.3-21.4-47.7-47.7-47.7h-53.4V17.8c0-9.6-7.8-17.4-17.4-17.4h-27.1c-9.6,0-17.4,7.8-17.4,17.4v29.5H163V17.8
                      c0-9.6-7.8-17.4-17.4-17.4h-27.1c-9.6,0-17.4,7.8-17.4,17.4v29.5H47.7C21.4,47.3,0,68.7,0,95v293.3C0,414.5,21.4,435.9,47.7,435.9
                      z M489.2,397.7c0,50.3-40.8,91.1-91.1,91.1S307,448,307,397.7s40.8-91.1,91.1-91.1S489.2,347.4,489.2,397.7z M444.1,374.1
                      c0-2.9-1.1-5.7-3.2-7.7c-4.3-4.3-11.2-4.3-15.5,0L385.8,406l-15.2-15.2c-4.3-4.3-11.2-4.3-15.5,0c-2.1,2.1-3.2,4.8-3.2,7.7
                      c0,2.9,1.1,5.7,3.2,7.7l22.9,22.9c4.3,4.3,11.2,4.3,15.5,0l47.3-47.3C443,379.8,444.1,377,444.1,374.1z"/>
                  </g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                </svg>
                <span>
                <?php echo $row_t_p['data_od'].'-'.$row_t_p['data_do'] ;?>
                </span>
              </div>
              <div class="address">
              <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  width="16" height="16" viewBox="0 0 491.582 491.582" style="enable-background:new 0 0 491.582 491.582;"
                  xml:space="preserve">
                <g>
                  <g>
                    <path d="M245.791,0C153.799,0,78.957,74.841,78.957,166.833c0,36.967,21.764,93.187,68.493,176.926
                      c31.887,57.138,63.627,105.4,64.966,107.433l22.941,34.773c2.313,3.507,6.232,5.617,10.434,5.617s8.121-2.11,10.434-5.617
                      l22.94-34.771c1.326-2.01,32.835-49.855,64.967-107.435c46.729-83.735,68.493-139.955,68.493-176.926
                      C412.625,74.841,337.783,0,245.791,0z M322.302,331.576c-31.685,56.775-62.696,103.869-64.003,105.848l-12.508,18.959
                      l-12.504-18.954c-1.314-1.995-32.563-49.511-64.007-105.853c-43.345-77.676-65.323-133.104-65.323-164.743
                      C103.957,88.626,167.583,25,245.791,25s141.834,63.626,141.834,141.833C387.625,198.476,365.647,253.902,322.302,331.576z"/>
                    <path d="M245.791,73.291c-51.005,0-92.5,41.496-92.5,92.5s41.495,92.5,92.5,92.5s92.5-41.496,92.5-92.5
                      S296.796,73.291,245.791,73.291z M245.791,233.291c-37.22,0-67.5-30.28-67.5-67.5s30.28-67.5,67.5-67.5
                      c37.221,0,67.5,30.28,67.5,67.5S283.012,233.291,245.791,233.291z"/>
                  </g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                </svg>
                <span>
                <?php echo $row_t_p['adres'];?>
                </span>
              </div>
            </div>
            <div class="info-training">
              <h3 class="title"><?php echo $row_t['szkolenie_wpis']->post_title;?></h3>
              <p class="text"><?php echo $row_t_p['text_podstrona_szkolen_oraz_glowna'];?></p>
            </div>
            <div class="w-cta">
              <a class="cta gold" href="<?php echo $row_t['szkolenie_wpis']->guid;?>" rel="follow">szczegóły</a>
            </div>
            
      </div>
      <?php endforeach ;?>
      </div>
     <div class="w-cta">
     <a href="<?php echo  $_section_training['hiperlacze_strona_szkolen'];?>" class="gold" rel="follow">zobacz wszystkie szkolenia fryzjerskie</a>
     </div>
      
  </section>
      <?php endif;// end trainin section?>
      <?php
  if(get_field('salony',10) !== null) :
    $_section_salons = get_field('salony',10);
  ?>
  </div><!-- end row --->
</div><!-- end w-content --->
</div><!-- end w-content --->
<section class="w-salons">
  <div class="header">
    <?php if($_section_salons['ikona_strona_glowna']): ;?>
    <img src="<?php echo $_section_salons['ikona_strona_glowna']['url'];?>" />
    <?php endif ;?>
      <h2 class="title"><span>Szkolenia</span><span class="gold">fryzjerskie</span></h2>
    <?php if($_section_salons['text_strona_glowna']): ;?>
      <div class="text"><?php echo $_section_salons['text_strona_glowna'];?></div>
    <?php endif ;?>
  </div>
  <div class="w-content">
    <div class="salons-content">
    <?php foreach ( $_section_salons['dane_tele_adresowe'] as $row_salons ): ;?>
      <div class="item-salons" >
        <?php if($row_salons['naglowek_nazwa']): ;?>
          <p class="title"><?php echo $row_salons['naglowek_nazwa'];?></p>
        <?php endif ;?>
        <?php if($row_salons['naglowek_adres']): ;?>
          <p class="text gold"><?php echo $row_salons['naglowek_adres'];?></p>
        <?php endif ;?>
        <?php if($row_salons['mapa']): ;?>
          <div class="maps">
          <iframe src="<?php echo trim($row_salons['mapa']);?>" frameborder="0" style="border:0;" allowfullscreen=""></iframe>  
         </div>
        <?php endif ;?>
        <?php if($row_salons['adres_pelny']): ;?>
          <div class="w-text">
            <p>Adres:</p>
            <p class="text"><?php echo $row_salons['adres_pelny'];?></p>
          </div>
        <?php endif ;?>
        <?php if($row_salons['telefon']): ;?>
          <div class="w-text">
            <p>Telefon:</p>
            <a class="text" href="tel:<?php echo trim($row_salons['telefon']);?>"><?php echo Trim($row_salons['telefon']);?></a>
          </div>
        <?php endif ;?>
        <?php if($row_salons['godziny_otwarcia']): ;?>
          <div class="w-text">
          <p>Godziny otwarcia:</p>
            <p class="text"><?php echo trim($row_salons['godziny_otwarcia']);?></p>
          </div>
        <?php endif ;?>
      </div>
      <?php endforeach ;?>
    </div>
  </div>
</section>
<section class="w-instagram">
  <div class="header">
          <img src="./wp-content/uploads/2019/09/title-img-4.png" alt="Instagram">
            <h2 class="title">Instagram</h2>
        </div>
  <?php echo do_shortcode('[instagram-feed]');?>
</section>

<div><!-- start row --->
<div><!--start w-content --->
<div><!-- start site-content --->
<?php
endif;
get_footer();