
<?php

if ( is_active_sidebar( 'footer-1' ) || is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) || is_active_sidebar( 'footer-4' ) || is_active_sidebar( 'footer-5' )) {?>
        <div id="footer-widget" class=" <?php if(!is_theme_preset_active()){ echo 'bg-light'; } ?>">
       
                    <?php if ( is_active_sidebar( 'footer-1' )) : ?>
                        <div class="w-f1"><?php dynamic_sidebar( 'footer-1' ); ?></div>
                    <?php endif; ?>
                    <?php if ( is_active_sidebar( 'footer-2' )) : ?>
                        <div class="w-f2"><?php dynamic_sidebar( 'footer-2' ); ?></div>
                    <?php endif; ?>
                    <?php if ( is_active_sidebar( 'footer-3' )) : ?>
                        <div class="w-f3"><?php dynamic_sidebar( 'footer-3' ); ?></div>
                    <?php endif; ?>
                    <?php if ( is_active_sidebar( 'footer-4' )) : ?>
                        <div class="w-f4"><?php dynamic_sidebar( 'footer-4' ); ?></div>
                    <?php endif; ?>
                    <?php if ( is_active_sidebar( 'footer-5' )) : ?>
                        <div class="w-f5"><?php dynamic_sidebar( 'footer-5' ); ?></div>
                    <?php endif; ?>
        
        </div>

<?php }