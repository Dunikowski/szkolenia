<?php
/**
 * Template Name: Salons
 */

get_header();

    $_section_salons = get_field('salony');
?>
   <section class="w-salons">
  <div class="header">
    <?php if($_section_salons['ikona_podstrona_salony']): ;?>
    <img src="<?php echo $_section_salons['ikona_podstrona_salony']['url'];?>" />
    <?php endif ;?>
      <h1 class="title">Salony</h1>
   
  </div>
  <div class="w-content">
    <div class="salons-content">
    <?php foreach ( $_section_salons['dane_tele_adresowe'] as $row_salons ): ;?>
      <div class="item-salons" >
          <div class="left">
          <?php if($row_salons['naglowek_nazwa']): ;?>
            <p class="title"><?php echo $row_salons['naglowek_nazwa'];?></p>
            <?php endif ;?>
            <?php if($row_salons['naglowek_adres']): ;?>
            <p class="text gold"><?php echo $row_salons['naglowek_adres'];?></p>
            <?php endif ;?>
            <?php if($row_salons['adres_pelny']): ;?>
          <div class="w-text">
            <p>Adres:</p>
            <p class="text"><?php echo $row_salons['adres_pelny'];?></p>
          </div>
        <?php endif ;?>
        <?php if($row_salons['telefon']): ;?>
          <div class="w-text">
            <p>Telefon:</p>
            <a class="text" href="tel:<?php echo trim($row_salons['telefon']);?>"><?php echo Trim($row_salons['telefon']);?></a>
          </div>
        <?php endif ;?>
        <?php if($row_salons['godziny_otwarcia']): ;?>
          <div class="w-text">
          <p>Godziny otwarcia:</p>
            <p class="text"><?php echo trim($row_salons['godziny_otwarcia']);?></p>
          </div>
        <?php endif ;?>
          </div>
      
        <?php if($row_salons['mapa']): ;?>
          <div class="maps">
          <iframe src="<?php echo trim($row_salons['mapa']);?>" frameborder="0" style="border:0;" allowfullscreen=""></iframe>  
         </div>
        <?php endif ;?>
      </div>
      <?php endforeach ;?>
    </div>
  </div>
</section>

<?php

get_footer();
