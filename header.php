<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'wp-bootstrap-starter' ); ?></a>
    <?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
	<header id="masthead" class="site-header navbar-static-top <?php echo wp_bootstrap_starter_bg_class(); ?>" role="banner">
        <div class="w-content">
            <nav class="navbar navbar-expand-xl p-0">
                <div class="navbar-brand">
                    <?php if ( get_theme_mod( 'wp_bootstrap_starter_logo' ) ): ?>
                        <a href="<?php echo esc_url( home_url( '/' )); ?>">
                            <img src="<?php echo esc_url(get_theme_mod( 'wp_bootstrap_starter_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                        </a>
                    <?php else : ?>
                        <a class="site-title" href="<?php echo esc_url( home_url( '/' )); ?>"><?php esc_url(bloginfo('name')); ?></a>
                    <?php endif; ?>

                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <?php
                wp_nav_menu(array(
                'theme_location'    => 'primary',
                'container'       => 'div',
                'container_id'    => 'main-nav',
                'container_class' => 'collapse navbar-collapse justify-content-start',
                'menu_id'         => false,
                'menu_class'      => 'navbar-nav',
                'depth'           => 3,
                'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                'walker'          => new wp_bootstrap_navwalker()
                ));
                ?>

            </nav>
        </div>
	</header><!-- #masthead -->
    <?php if(is_front_page() && get_field('top_front_page', 18) !== null ): ?>
       
        <div class="w-section-top" >
                   
            <?php  
            $top_baner = get_field('top_front_page', 18); 
            
            ;?>
            <div class="top flex-align-vertical">
            <picture>
                <source media="(min-width: 1280px)" srcset="<?php echo  $top_baner['tlo_gorne_desktop']['url']; ?>">
                <source media="(min-width: 700px)" srcset="<?php echo $top_baner['tlo_gorne_tablet']['url']; ?>">
                <source media="(min-width: 300px)" srcset="<?php echo $top_baner['tlo_gorne_smartfon']['url']; ?>">
                <img src="<?php echo $top_baner['tlo_gorne_desktop']['url']; ?>">
                </picture>
                <div class="w-content">
                <div class="title-page flex-align-vertical">
                <img class="abs" src="<?php echo $top_baner['ikona_1']['url'];?>" >   
                <div class="w-name-page">
                    <img src="<?php echo $top_baner['tytul_ikona']['url']; ?>">
                        <?php
                            if($top_baner['tytul_strony']) {
                            echo $top_baner['tytul_strony'];
                            }
                        ?>
                    </div>
                </div>
            </div>
            </div>
            <?php if($top_baner['workplace'] || $top_baner['ikona_2']):;?>
                <div class="workplace w-content">
                    <div class="icon">
                        <img src="<?php echo $top_baner['ikona_2']['url']; ?>" />
                    </div>
                        <?php echo apply_filters('the_content',get_post_field('post_content',18));?>
                </div>
            <?php endif;?>
            <div class="bg-image">
                <div class="w-content">
                <picture>
                    <source media="(min-width: 1280px)" srcset="<?php echo $top_baner['popiersie_desktop']['url']; ?>">
                    <source media="(min-width: 700px)" srcset="<?php echo $top_baner['popiersie_tablet']['url']; ?>">
                    <source media="(min-width: 300px)" srcset="<?php echo $top_baner['popiersie_smartfon']['url']; ?>">
                    <img src="<?php echo $top_baner['popiersie_desktop']['url']; ?>">
                </picture>
                </div>
            </div>
                   
        </div>
    <?php endif; ?>
                        <div id="content" class="site-content" <?php if(is_front_page() && get_field('sekcja_zespol',6)['tlo__sekcji_zespol'] !== null ): ?>style="background: url(' <?php echo get_field('sekcja_zespol',6)['tlo__sekcji_zespol']['url'];?>') no-repeat" <?php endif; ?>>
		<div class="w-content">
			<div class="row">
                <?php endif; ?>