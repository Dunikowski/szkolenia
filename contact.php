<?php
/**
 * Template Name: Contact
 */

get_header();
if(get_field('kontakt') !== null) :
    $_section_contact = get_field('kontakt'); 
?>
<section class="contact">
  <div class="w-content">
    <div class="info">
      <div class="heading">
        <div class="icon">
          <?php 
                        if( $_section_contact['ikona']){
                            echo '<img src="'.$_section_contact['ikona']['url'].'"/ alt="'.$_section_contact['ikona']['alt'].'">';
                        } 
                        ;?>
        </div>
        <h1 class="title">Kontakt</h1>
      </div>
      <p class="title">Dane firmy</p>
      <?php 
      
      if($_section_contact['adresy_e-mail']):;?>
      <p class="title fw-400">Hair Team Łukasz Urbański</p>
      <?php foreach ( $_section_contact['adresy_e-mail'] as $row ): 
      
        ;?>

      <div class="text">
        <svg version="1.1" width="16" height="16" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 483.3 483.3" style="enable-background:new 0 0 483.3 483.3;" xml:space="preserve">
          <g>
            <g>
              <path d="M424.3,57.75H59.1c-32.6,0-59.1,26.5-59.1,59.1v249.6c0,32.6,26.5,59.1,59.1,59.1h365.1c32.6,0,59.1-26.5,59.1-59.1
                                v-249.5C483.4,84.35,456.9,57.75,424.3,57.75z M456.4,366.45c0,17.7-14.4,32.1-32.1,32.1H59.1c-17.7,0-32.1-14.4-32.1-32.1v-249.5
                                c0-17.7,14.4-32.1,32.1-32.1h365.1c17.7,0,32.1,14.4,32.1,32.1v249.5H456.4z" />
              <path d="M304.8,238.55l118.2-106c5.5-5,6-13.5,1-19.1c-5-5.5-13.5-6-19.1-1l-163,146.3l-31.8-28.4c-0.1-0.1-0.2-0.2-0.2-0.3
                                c-0.7-0.7-1.4-1.3-2.2-1.9L78.3,112.35c-5.6-5-14.1-4.5-19.1,1.1c-5,5.6-4.5,14.1,1.1,19.1l119.6,106.9L60.8,350.95
                                c-5.4,5.1-5.7,13.6-0.6,19.1c2.7,2.8,6.3,4.3,9.9,4.3c3.3,0,6.6-1.2,9.2-3.6l120.9-113.1l32.8,29.3c2.6,2.3,5.8,3.4,9,3.4
                                c3.2,0,6.5-1.2,9-3.5l33.7-30.2l120.2,114.2c2.6,2.5,6,3.7,9.3,3.7c3.6,0,7.1-1.4,9.8-4.2c5.1-5.4,4.9-14-0.5-19.1L304.8,238.55z" />
            </g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
        </svg>
        <a class="text" href="mailto:<?php echo $row['e-mail'];?>"><?php echo $row['e-mail'];?></a>
      </div>
      <?php endforeach;?>
      <?php endif;?>
      <?php if($_section_contact['nr_telefonow']):;?>
      <?php foreach ( $_section_contact['nr_telefonow'] as $row_phone ): ;?>
      <div class="text phone">
        <svg version="1.1" width="16" height="16" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
          <g>
            <g>
              <path d="M498.827,377.633l-63.649-63.649c-17.548-17.547-46.102-17.547-63.649,0l-28.931,28.931
                                c-13.294,13.294-34.926,13.29-48.215,0.005l-125.4-125.507c-13.325-13.325-13.327-34.892,0-48.219
                                c4.66-4.66,18.041-18.041,28.931-28.931c17.471-17.47,17.715-45.935-0.017-63.665l-63.632-63.432
                                C116.717-4.381,88.164-4.381,70.663,13.12C57.567,26.102,53.343,30.29,47.471,36.111c-63.28,63.279-63.28,166.242-0.003,229.519
                                l198.692,198.796c63.428,63.429,166.088,63.434,229.521,0l23.146-23.145C516.375,423.733,516.375,395.181,498.827,377.633z
                                M91.833,34.382c5.849-5.849,15.365-5.85,21.233,0.016l63.632,63.432c5.863,5.863,5.863,15.352,0,21.216l-10.609,10.608
                                l-84.81-84.81L91.833,34.382z M267.38,443.213L68.687,244.415c-48.958-48.958-51.649-125.833-8.276-178.006l84.564,84.564
                                c-22.22,25.189-21.294,63.572,2.787,87.653l125.396,125.501c0.001,0.001,0.003,0.003,0.004,0.004
                                c24.055,24.056,62.436,25.042,87.656,2.792l84.566,84.566C393.377,494.787,316.675,492.508,267.38,443.213z M477.612,420.065
                                l-10.609,10.609l-84.865-84.866l10.607-10.608c5.85-5.849,15.367-5.85,21.217,0l63.649,63.649
                                C483.461,404.699,483.461,414.217,477.612,420.065z" />
            </g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
        </svg>
        <a class="text " href="tel:<?php echo $row_phone ['telefon'];?>"><?php echo $row_phone['telefon'];?></a>
      </div>
      <?php endforeach;?>
      <?php endif;?>
      <?php if($_section_contact['adres_glownej_siedziby']):;?>
      <div class="head-office-address">
        <p class="title fw-400">
          Adres głownej siedziby
        </p>
        <div class="w-office-adress text">
        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            width="16" height="16" viewBox="0 0 491.582 491.582" style="enable-background:new 0 0 491.582 491.582;"
            xml:space="preserve">
            <g>
            <g>
                <path d="M245.791,0C153.799,0,78.957,74.841,78.957,166.833c0,36.967,21.764,93.187,68.493,176.926
                c31.887,57.138,63.627,105.4,64.966,107.433l22.941,34.773c2.313,3.507,6.232,5.617,10.434,5.617s8.121-2.11,10.434-5.617
                l22.94-34.771c1.326-2.01,32.835-49.855,64.967-107.435c46.729-83.735,68.493-139.955,68.493-176.926
                C412.625,74.841,337.783,0,245.791,0z M322.302,331.576c-31.685,56.775-62.696,103.869-64.003,105.848l-12.508,18.959
                l-12.504-18.954c-1.314-1.995-32.563-49.511-64.007-105.853c-43.345-77.676-65.323-133.104-65.323-164.743
                C103.957,88.626,167.583,25,245.791,25s141.834,63.626,141.834,141.833C387.625,198.476,365.647,253.902,322.302,331.576z"/>
                <path d="M245.791,73.291c-51.005,0-92.5,41.496-92.5,92.5s41.495,92.5,92.5,92.5s92.5-41.496,92.5-92.5
                S296.796,73.291,245.791,73.291z M245.791,233.291c-37.22,0-67.5-30.28-67.5-67.5s30.28-67.5,67.5-67.5
                c37.221,0,67.5,30.28,67.5,67.5S283.012,233.291,245.791,233.291z"/>
            </g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            </svg>
          <div class="w-address">
          <?php if($_section_contact['adres_glownej_siedziby']['ulica']):;?>
            <p class="street"><?php echo $_section_contact['adres_glownej_siedziby']['ulica'];?></p>
          <?php endif;?>
          <?php if($_section_contact['adres_glownej_siedziby']['kod_pocztowy_miasto']):;?>
            <p class="city"><?php echo $_section_contact['adres_glownej_siedziby']['kod_pocztowy_miasto'];?></p>
            <?php endif;?>
          </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($_section_contact['adres_szkolen']):;?>
      <div class="training-address">
        <p class="title fw-400">
          Adres szkoleń
        </p>
        <div class="w-office-adress text">
        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            width="16" height="16" viewBox="0 0 491.582 491.582" style="enable-background:new 0 0 491.582 491.582;"
            xml:space="preserve">
            <g>
            <g>
                <path d="M245.791,0C153.799,0,78.957,74.841,78.957,166.833c0,36.967,21.764,93.187,68.493,176.926
                c31.887,57.138,63.627,105.4,64.966,107.433l22.941,34.773c2.313,3.507,6.232,5.617,10.434,5.617s8.121-2.11,10.434-5.617
                l22.94-34.771c1.326-2.01,32.835-49.855,64.967-107.435c46.729-83.735,68.493-139.955,68.493-176.926
                C412.625,74.841,337.783,0,245.791,0z M322.302,331.576c-31.685,56.775-62.696,103.869-64.003,105.848l-12.508,18.959
                l-12.504-18.954c-1.314-1.995-32.563-49.511-64.007-105.853c-43.345-77.676-65.323-133.104-65.323-164.743
                C103.957,88.626,167.583,25,245.791,25s141.834,63.626,141.834,141.833C387.625,198.476,365.647,253.902,322.302,331.576z"/>
                <path d="M245.791,73.291c-51.005,0-92.5,41.496-92.5,92.5s41.495,92.5,92.5,92.5s92.5-41.496,92.5-92.5
                S296.796,73.291,245.791,73.291z M245.791,233.291c-37.22,0-67.5-30.28-67.5-67.5s30.28-67.5,67.5-67.5
                c37.221,0,67.5,30.28,67.5,67.5S283.012,233.291,245.791,233.291z"/>
            </g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            </svg>
          <div class="w-address">
          <?php if($_section_contact['adres_szkolen']['ulica']):;?>
            <p class="street"><?php echo $_section_contact['adres_szkolen']['ulica'];?></p>
          <?php endif;?>
          <?php if($_section_contact['adres_szkolen']['kod_pocztowy_miasto']):;?>
            <p class="city"><?php echo $_section_contact['adres_szkolen']['kod_pocztowy_miasto'];?></p>
            <?php endif;?>
          </div>
        </div>
      </div>
      <?php endif;?>
    </div>
    <div class="map">
    <div class="title">
        Mapa głównej siedziby
      </div>
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2444.172481529995!2d21.01378351602957!3d52.22208407975886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x471eccef0ec27075%3A0x5fd41689c63c38f4!2splac%20Konstytucji%2C%2000-001%20Warszawa!5e0!3m2!1sen!2spl!4v1569591169427!5m2!1sen!2spl" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>
    <div class="w-form">
      <div class="title">
        Formularz kontaktowy
      </div>
      <?php echo do_shortcode('[contact-form-7 id="238" title="Contact form 1"]');?>
    </div>
  </div>
</section><!-- #primary -->

<?php
endif;
get_footer();