<?php
/**
 * Template Name: Hair Team
 */

get_header();

if(get_field('sekcja_zespol') !== null) :
    $_section_team = get_field('sekcja_zespol');  
    $_section_team_enlargement = get_field('top_front_page',18);
?>
<section class="hair-team-top">
  <div class="w-content-hair-team">
    <div class="left-column">
      <div class="team-content">
        <div class="heading">
          <div class="icon">
            <?php 
                if($_section_team['ikona']){
                    echo '<img src="'.$_section_team['ikona']['url'].'"/>';
                } 
                ;?>
          </div>
          <h1 class="title">
            <?php 
                if($_section_team['ikona']){
                    echo $_section_team['tytul'];
                } 
                ;?>
          </h1>
        </div>
        <p class="title">
          <?php 
                if($_section_team['imie_i_nazwisko']){
                    echo $_section_team['imie_i_nazwisko'];
                } 
                ;?>
        </p>
        <p class="position gold">
          <?php 
                if($_section_team['stanowisko']){
                    echo $_section_team['stanowisko'];
                } 
                ;?>
        </p>
            <?php
                while ( have_posts() ) : the_post();
                the_content();
                endwhile; // End of the loop.
            ?>

      </div>

    </div>
    <div class="right-column">
      <div class="bg-image">
        <picture>
        <source media="(min-width: 1280px)" srcset="<?php echo $_section_team_enlargement['popiersie_desktop']['url']; ?>">
          
          <source media="(min-width: 700px)" srcset="<?php echo $_section_team_enlargement['popiersie_tablet']['url']; ?>">
          <source media="(min-width: 300px)" srcset="<?php echo $_section_team_enlargement['popiersie_smartfon']['url']; ?>">
          <img src="<?php echo $_section_team_enlargement['popiersie_desktop']['url']; ?>">
        </picture>

      </div>
    </div>
  </div>
</section><!-- #primary -->

</div><!-- end row --->
</div>
<!--end w-content --->
</div><!-- end site-content --->
<?php if( get_field('sekcja_zespol')['workeres']) : ;?>

<div class="team-persons">

  <?php foreach ( $_section_team['workeres'] as $row ): ;?>
  <div class="item-person" style="background: url(' <?php echo $row['tlo']['url'];?>') no-repeat">
    <div class="w-info">
      <p class="name"><?php echo $row['name'];?></p>
      <p class="alias"><?php echo $row['alias'];?></p>
      <p class="text fw-400 fs-16"><?php echo $row['text'];?></p>
      <p class="text fw-300 fs-14"><?php echo $row['dluzszy_text'];?></p>
    </div>
  </div>
  <?php endforeach ;?>
</div>
<div>
  <!-- start row --->
  <div>
    <!--start w-content --->
    <div>
      <!-- start site-content --->
      <?php endif;
endif;
get_footer();