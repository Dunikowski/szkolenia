<?php
/**
 * Template Name: Models
 */

get_header();
$_site_models = get_field('modelki_i_modele');
?>
    <section class="w-models">
        <main id="main" class="site-main" role="main">
            <div class="w-columns">
            <div class="left-column">
                <div class="header">
                <img src="<?php echo $_site_models['ikona_strony']['url'] ;?>" alt="<?php echo $_site_models['ikona_strony']['alt'] ;?>" class="icon">
                <h1 class="title">Modelki & Modele</h1>

                </div>
                <div class="w-content-models">
                <?php
                while ( have_posts() ) : the_post();
                   the_content();
                endwhile; // End of the loop.
                ?>
                </div>
          
            </div>
            <div class="form">
                <p class="title">Formularz zgłoszeniowy</p>
                <?php echo do_shortcode( '[contact-form-7 id="239" title="Modelki & Modele"]' );?>
            </div>
            </div>
           <div class="w-gallery">
               <div class="header">
               <img src="<?php echo $_site_models['ikona_galerii']['url'] ;?>" alt="<?php echo $_site_models['ikona_strony']['alt'] ;?>" class="icon">
                <p class="title">Modelki & Modele</p>
               </div>
               <?php if($_site_models['galeria']) :;?>
                <div class="gallery">
                    
                <?php foreach ( $_site_models['galeria']as $row_gallery ): ;?>
                    <div class="item-gallery bg-display-image" style="background: url('<?php echo $row_gallery ['url'];?>') no-repeat">
                       
                    </div>
                <?php endforeach ;?>
                
                </div>
                <?php endif;?>
           </div>
        </main><!-- #main -->
    </section><!-- #primary -->

<?php
get_footer();
